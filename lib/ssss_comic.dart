// import 'package:sqflite/sqflite.dart';
// import 'package:html/parser.dart';
// import 'package:html/dom.dart';
// import 'package:http/http.dart' as http;
// import 'dart:async';
import 'comic_manager.dart';


class SSSSManager {
	
	String _name = "Stand Still Stay Silent";
	String _website = "http://sssscomic.com/index.php";
	String _comicPageUrlStub = "http://sssscomic.com/comic.php?page=";
	String _imageUrl = "http://sssscomic.com/comicpages/";
	static const int comicId = 1;
	int latestAddedPage = 973;

	String get name => _name;
	String get website => _website;

	SSSSManager();


	// Future<Map<String, dynamic>> _getLatestPage() async {
	// 	var response = await http.get(_latestComicUrl);
	// 	if(response.statusCode != 200) {
	// 		return {"num": 0};
	// 	} else {
	// 		var page = parse(response.body);
	// 		String title = page.getElementsByTagName("title")[0].innerHtml;
	// 		String strippedTitle = title.replaceAll(RegExp(r'''[^0123456789]'''), "");
	// 		int pageNumber = int.tryParse(strippedTitle) ?? 0;

	// 		return {"num": pageNumber, "pageResponse": response};
	// 	}
	// }

	ComicPage getPage(num pageNumber) {
		if(pageNumber < 1) return null;

		String pageUrl = _comicPageUrlStub + pageNumber.toString();
		String imageUrl = _imageUrl + pageNumber.toString() + ".jpg";

		return ComicPage(
			pageNumber,
			imageUrl,
			pageUrl
		);
	}
}


String dateStringToUnixTime(String s) {
	s = s.trim();
	s = s.toLowerCase();
	s = s.replaceAll(RegExp(r'''[^0123456789abcdefghijklmnopqrstuvwxyz ]'''), "");
	var split = s.split(" ");

	int dayInt = int.parse(split[0]);
	String day = dayInt < 10 ? "0" + split[0] : split[0];
	String year = split[2];
	String month;
	
	switch(split[1]) {
		case "jan":
		case "january":
			month = "01";
			break;
		
		case "feb":
		case "february":
			month = "02";
			break;

		case "mar":
		case "march":
			month = "03";
			break;

		case "apr":
		case "april":
			month = "04";
			break;

		case "may":
			month = "05";
			break;

		case "jun":
		case "june":
			month = "06";
			break;

		case "jul":
		case "july":
			month = "07";
			break;

		case "aug":
		case "august":
			month = "08";
			break;

		case "sep":
		case "september":
			month = "09";
			break;

		case "oct":
		case "october":
			month = "10";
			break;

		case "nov":
		case "november":
			month = "11";
			break;

		case "dec":
		case "december":
			month = "12";
			break;

		default:
			throw FormatException("SSSS Comic date of unknown format");
	}

	var newDateString = year + '-' + month + '-' + day;
	var datetime = DateTime.parse(newDateString);
	return datetime.millisecondsSinceEpoch.toString();
}

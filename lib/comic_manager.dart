import 'database_manager.dart';

import 'comic.dart';

class ComicManager {
	List<Comic> comics;
	final DatabaseManager dm;

	ComicManager._(this.dm);

	static Future<ComicManager> create(DatabaseManager dm) async {
		var cm = ComicManager._(dm);
		cm.comics = [
			SSSSComic(dm),			
			SSSS2Comic(dm),
			XKCD(dm),		
		];

		for(var comic in cm.comics) {
			comic.init();
		}

		cm.dm.addComics(cm.comics);
		return cm;
	}

	void updateAll() {
		for(var comic in comics) {
			comic.updateComic();
		}
	}

}

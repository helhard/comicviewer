import 'package:sqflite/sqflite.dart';
import 'comic.dart';

class DatabaseManager {
	Database db;
	DatabaseManager._();
	
	static Future<DatabaseManager> create() async {
		var dm = DatabaseManager._();
		var tmpPath = await getDatabasesPath();
		var dbPath = tmpPath + "comics.db";
		dm.db = await openDatabase(dbPath, version: 1,
			onCreate: (db, version) async {
				await db.execute("""
					CREATE TABLE comics (
					id INTEGER PRIMARY KEY,
					name TEXT UNIQUE,
					pages INTEGER);
				""");
				await db.execute("""
					CREATE TABLE pages (
					id INTEGER PRIMARY KEY,
					comicId INTEGER REFERENCES comics(id),
					pageNum INTEGER,
					pageUrl TEXT UNIQUE,
					imageUrl TEXT UNIQUE,
					hiResImageUrl TEXT UNIQUE,
					title TEXT,
					description TEXT,
					UNIQUE(comicId, pageNum));
				""");
			},
		);
		return dm;
	}

	void addComics(List<Comic> comics) {
		for(var comic in comics) {
			db.rawInsert(
				"INSERT OR IGNORE INTO comics(id, name, pages) VALUES(?, ?, ?);",
				[comic.id, comic.name, comic.latestPage]
			);
		}
	}

	void updateComic(String name, int latest) async {
		var query =	"UPDATE comics SET pages = ? WHERE name = ?;";
		db.rawInsert(query, [latest, name]);
	}

	Future<Map<String, int>> initComic(String name) async {
		var query = "SELECT id, pages FROM comics WHERE name = ?;";
		var results = await db.rawQuery(query, [name]);
		if(results.length <= 0) return {"id": 0, "latestPage": 0};
		return {
			"id": results[0]["id"],
			"latestPage": results[0]["pages"]
		};
	}

	Future<void> addPage(
		int pageNum,
		int comicId,
		String title,
		String description,
		String imageUrl,
		String hiRezUrl,
		String pageUrl
	) async {
		await db.rawInsert(
			"""INSERT OR REPLACE INTO 
			pages(
				pageNum,
				comicId,
				title,
				description,
				imageUrl,
				pageUrl,
				hiResImageUrl
			) 
			VALUES(?, ?, ?, ?, ?, ?, ?);""",
			[pageNum, comicId, title, description, imageUrl, pageUrl, hiRezUrl]
		);
	}

	Future<Map<String, String>> getPageUrls(int pageNum, int comicId) async {
		var result = await db.rawQuery(
			"""SELECT  imageUrl, pageUrl, title, description, hiResImageUrl
			FROM pages WHERE pageNum = ? AND comicId = ?;""",
			[pageNum, comicId]
		);
		if(result.length > 0) {
			return {
				"pageUrl": result[0]["pageUrl"],
				"imageUrl": result[0]["imageUrl"],
				"title": result[0]["title"],
				"description": result[0]["description"],
				"hiResImageUrl": result[0]["hiResImageUrl"]
			};
		}
		return {
			"pageUrl": "",
			"imageUrl": "",
			"title": "",
			"description": "",
			"hiResImageUrl": ""
		};
	}
}
import 'package:flutter/material.dart';
import 'package:zoomable_image/zoomable_image.dart';
import 'comic_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'database_manager.dart';

import 'comic.dart';


// import 'package:flutter/rendering.dart';

void main() async {
	// debugPaintSizeEnabled=true;
	var databaseManager = await DatabaseManager.create();
	var comicManager = await ComicManager.create(databaseManager);
	runApp(MyApp(comicManager));
}

class FrontPage extends StatelessWidget {
	final ComicManager cm;
	FrontPage(this.cm);

	Widget build(context) {
		return MaterialApp(
			title: "Comic Viewer",
			home: Scaffold(
				body: ListView.builder(
					itemCount: cm.comics.length,
					itemBuilder: (context, index) {
						var comic = cm.comics[index];
						return ComicCard(comic);
					}
				),
			),
		);
	}
}




class MyApp extends StatelessWidget {
	final ComicManager cm;

	MyApp(this.cm);

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Flutter Demo',
			theme: ThemeData(
				primarySwatch: Colors.blue,
			),
			// home: Updates(this.cm, 1),
			home: FrontPage(this.cm),
		);
	}
}




class Updates extends StatefulWidget {
	Updates(this.comic);

	@override
	_UpdatesState createState() => _UpdatesState();

	final Comic comic;
}

class _UpdatesState extends State<Updates> {
	bool newestPageFirst = true;
	bool updateQueued = false;
	var controller = ScrollController(initialScrollOffset: 0.0);

	Future<void> _update() async {
		await widget.comic.updateComic();
		setState(() {
			updateQueued = false;
		});
	}

	@override
	Widget build(BuildContext context) {

		var pageList = ListView.builder(
			itemCount: widget.comic.latestPage,
			itemBuilder: (BuildContext context, int index){
				// // gotta use futurebuilder
				// var card = widget.comic.getPageForList(index, newestPageFirst);
			 //  return ComicPageCard();
			 // return Text("$index");
				return FutureBuilder<ComicPage> (
					future: widget.comic.getPageForList(index, newestPageFirst),
					builder: (BuildContext context, AsyncSnapshot snapshot) {
						if(snapshot.connectionState == ConnectionState.done) {
							if(snapshot.hasError) {return Text('Error: ${snapshot.error}');}
							return ComicPageCard(snapshot.data);
						} else {
							return Container(height: 190.0);
						}
					}
				);
			},
			controller: controller,
		);

		return Scaffold(
			appBar: AppBar(
				backgroundColor: updateQueued ? Colors.red : Colors.blue,
				title: Text(widget.comic.name),
				actions: <Widget>[
					PopupMenuButton<bool>(
						onSelected: (bool result) { setState(() {
							this.newestPageFirst = result; 
							pageList.controller.jumpTo(0.0);
						});},
						itemBuilder: (context) => <PopupMenuEntry<bool>>[
							const PopupMenuItem<bool> (
								value: true,
								child: Text("Newest First")
							),
							const PopupMenuItem<bool> (
								value: false,
								child: Text("Oldest First")
							),
						]
					)
				],
			),
			body: RefreshIndicator(
				child: pageList,
				onRefresh: () => _update(),
			),
		);
	}
}




class ComicPageCard extends StatelessWidget {
	final ComicPage page;
	const ComicPageCard(this.page);

	@override
	Widget build(BuildContext context) {
		return MyCard(
			child: GestureDetector(
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.start,
					children: [
						Container(
							child: Image(image: this.page.imageProvider, fit: BoxFit.fitWidth),
							color: Colors.black12,
							constraints: BoxConstraints.expand(height: 170.0),
						),
						Container(
							padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 10.0),
							child: Text(
								this.page.title,
								style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 1.4)
							)
						),
						Container(
							padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 10.0),
							child: Text(
								this.page.pageNumber.toString(),
								style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 0.8)
							)
						)
					]
				),
				onTap: () {
					var imageProv = this.page.hasHiRes ? this.page.hiResImageProvider : this.page.imageProvider;
					Navigator.push(
						context,
						MaterialPageRoute(
							builder: (context) => ZoomableImage(
								imageProv,
								placeholder: Container(color: Colors.black12)
							)
						)
					);
				}
			),
		);
	}
}




class ComicCard extends StatelessWidget {
	final Comic comic;
	ComicCard(this.comic);

	@override
	Widget build(BuildContext context) {
		return MyCard(
			child: GestureDetector(
				child: Column(
					children: [
						Container(
							child: Image(image: CachedNetworkImageProvider(comic.heroImageUrl), fit: BoxFit.fitWidth),
							padding: EdgeInsets.only(bottom: 20.0),
						),
						Container(
							child: Text(
								comic.name,
								textAlign: TextAlign.center,
								style: TextStyle(fontSize: 28.0), 
							),
							padding: EdgeInsets.only(bottom: 20.0)
						),
					],
				),
				onTap: () {
					Navigator.push(
						context,
						MaterialPageRoute(
							builder: (context) => Updates(comic),
						)
					);
				}
			),
		);
	}
}



class MyCard extends Card {
		MyCard({
		Key key,
		Widget child,
		EdgeInsetsGeometry margin = const EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
		}) : super(key: key, child: child, margin: margin);
}
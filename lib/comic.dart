import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/painting.dart';
import 'database_manager.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart';
import 'package:html/dom.dart';
import 'comic_manager.dart';



class ComicPage {
	num pageNumber;
	String imageUrl;
	String postUrl;
	String hiResImageUrl;
	String title;
	String description;
	bool hasHiRes = false;
	ImageProvider imageProvider;
	ImageProvider hiResImageProvider;

	ComicPage(
		this.pageNumber,
		this.imageUrl,
		this.postUrl,
		[this.title,
		this.description,
		this.hiResImageUrl]
	) {
		imageProvider = CachedNetworkImageProvider(imageUrl);
		if(this.hiResImageUrl != null && this.hiResImageUrl != ""){
			hiResImageProvider = CachedNetworkImageProvider(hiResImageUrl);
			this.hasHiRes = true;
		}
	}
}


abstract class Comic {
	DatabaseManager _dm;
	String _name;
	String _homePage;
	String _heroImageUrl;
	int _latestPage = 1;
	int _id;

	String get name => _name;
	String get homePage => _homePage;
	String get heroImageUrl => _heroImageUrl;
	int get latestPage => _latestPage;
	int get id => _id;

	Future<void> init() async {
		var r = await _dm.initComic(this.name);
		_latestPage = r["latestPage"];
		await updateComic();
	}

	Future<void> updateComic();
	// Future<String> _updatePageInfo(int pageNumber);
	Future<ComicPage> getPage(int pageNumber);

	Future<ComicPage> getPageForList(int index, bool newestFirst) async {
		// index is the index of listView and the top starts at zero
		// so this maps the index to a page depending on wether it should be ordered
		// oldest first or newest first
		int pageNumber = newestFirst ? _latestPage - index : index + 1;
		return getPage(pageNumber);
	}
}

class SSSS2Comic extends Comic {
	SSSS2Comic(DatabaseManager dm){
		this._id = 1;
		this._dm = dm;
		this._name = "Stand Still Stay Silent Adv. 2";
		this._homePage = "http://sssscomic.com/index.php";
		this._heroImageUrl = "http://sssscomic.com/bgbig2.jpg";
	}

	@override
	Future<void> updateComic() async {
		int currentPage = _latestPage;
		var response = await http.get("http://sssscomic.com/comic2.php");
		if(response.statusCode == 200) {
			var page = parse(response.body);
			var elements = page.getElementsByClassName("pagenum");
			if(elements.length < 1) return;

			var p = Element.html(elements[0].innerHtml);
			int pageNumber = int.tryParse(p.innerHtml.trim());
			if(pageNumber <= currentPage) return;

			_dm.updateComic(_name, pageNumber);
			_latestPage = pageNumber; 
		}
	}

	@override
	Future<ComicPage> getPage(int pageNumber) async {
		return ComicPage(
			pageNumber,
			"http://www.sssscomic.com/adv2_comicpages/page_$pageNumber.jpg",
			"http://sssscomic.com/comic2.php?page=$pageNumber",
			"Page $pageNumber",
		);
	}
}



class SSSSComic extends Comic{
	SSSSComic(DatabaseManager dm){
		this._id = 2;
		this._dm = dm;
		this._name = "Stand Still Stay Silent";
		this._homePage = "http://sssscomic.com/index.php";
		this._heroImageUrl = "http://sssscomic.com/bgbig.jpg";
	}

	Future<void> init() async {
		this._latestPage = 973;
	}

	@override
	Future<void> updateComic() async {} // this comic is finished

	@override
	Future<ComicPage> getPage(int pageNumber) async {
		return ComicPage(
			pageNumber,
			"http://sssscomic.com/comicpages/$pageNumber.jpg",
			"http://sssscomic.com/comic.php?page=$pageNumber",
			"Page $pageNumber"
		);
	}
}



class XKCD extends Comic {
	XKCD(DatabaseManager dm){
		this._id = 3;
		this._dm = dm;
		this._name = "XKCD";
		this._homePage = "https://www.xkcd.com/";
		this._heroImageUrl = "https://www.xkcd.com/s/0b7742.png";
	}
	

	@override
	Future<void> updateComic() async {
		int currentPage = _latestPage;
		var response = await http.get("https://www.xkcd.com");
		if(response.statusCode == 200) {
			var page = parse(response.body);
			var elements = page.getElementsByClassName("comicNav");
			if(elements.length < 1) return;
			var nav = elements[0];
			for(var ul in nav.children) {
				if(ul.firstChild.attributes["rel"] == "prev") {
					String previousPageNum = ul.firstChild.attributes["href"].replaceAll("/", "");
					int pageNumber = int.tryParse(previousPageNum) + 1;
					if(pageNumber <= currentPage) return;
					_dm.updateComic(_name, pageNumber);
					_latestPage = pageNumber; 
				}
			}
		}
	}

	@override
	Future<ComicPage> getPage(int pageNumber) async {
		var res = await _dm.getPageUrls(pageNumber, this.id);
		// if we have the page in the db return it
		String title = "";
		String description = "";
		String imageUrl = "";
		String hiResUrl = "";
		var pageUrl = "https://www.xkcd.com/$pageNumber/";
		if(res["pageUrl"] != "") {
			imageUrl = res["imageUrl"];
			pageUrl = res["pageUrl"];
			hiResUrl = res["highRezImageUrl"];
			title = res["title"];
			description = res["description"];
		} else { // if we don't have the page in db add it to the db before returning it
			var response = await http.get(pageUrl);
			if(response.statusCode == 200) {
				var page = parse(response.body);
				var comic = page.getElementById("comic");
				if(comic.children.length == 1
					&& comic.children[0].localName == "img"
					&& comic.children[0].attributes["title"] != null) {
				// if I found the img element use info from that
					var img = comic.children[0];
					imageUrl = "https:" + img.attributes["src"].toString();
					title = img.attributes["alt"];
					description = img.attributes["title"];
					hiResUrl = "https:" + img.attributes["srcset"].split(" ")[0].trim();
				} else {
				// else parse the page for the url and name and stuff
					String middle = page.getElementById("middleContainer").innerHtml;
					int index = middle.indexOf("Image URL (");
					int start = index + 38; // there are 38 letters from index to where the url starts
					int end = middle.indexOf("\n", index);
					imageUrl = middle.substring(start, end);
					String tmp = imageUrl.replaceAll(".png", "_2x.png");
					var hiResCheck = await http.get(tmp);
					if(hiResCheck.statusCode == 200) hiResUrl = tmp;
					title = page.getElementById("ctitle").innerHtml.toString().trim();
				}
				await _dm.addPage(
					pageNumber,
					this.id,
					title,
					description,
					imageUrl,
					pageUrl,
					hiResUrl
				);
			}
		}
		return ComicPage(
			pageNumber,
			imageUrl,
			pageUrl,
			title,
			description,
			hiResUrl,
		);

	}
}
